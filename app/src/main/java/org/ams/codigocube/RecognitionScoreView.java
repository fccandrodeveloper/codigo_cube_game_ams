/* Copyright 2015 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

package org.ams.codigocube;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import org.ams.codigocube.Classifier.Recognition;

import java.util.List;

public class RecognitionScoreView extends View {
  private static final float TEXT_SIZE_DIP = 24;
  private List<Recognition> results;
  private final float textSizePx;
  private final Paint fgPaint;
  private final Paint bgPaint;
  private boolean findImage;
  private Context context;

  public RecognitionScoreView(final Context context, final AttributeSet set) {
    super(context, set);
    this.context = context;

    textSizePx =
        TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
    fgPaint = new Paint();
    fgPaint.setTextSize(textSizePx);

    bgPaint = new Paint();
    bgPaint.setColor(0xcc4285f4);
  }

  public void setResults(final List<Recognition> results) {
    this.results = results;
    postInvalidate();
  }

  @Override
  public void onDraw(final Canvas canvas) {
    final int x = 10;
    int y = (int) (fgPaint.getTextSize() * 1.5f);
//    int y = (int) (fgPaint.getTextSize());
    canvas.drawPaint(bgPaint);
    Intent intent = new Intent(getContext(), WebViewGame.class);
    if (results != null) {
      for (final Recognition recog : results) {
        Log.d("creation", "ID===" + recog.getId() + ",     title=" + recog.getTitle() + ",      percentage= " + recog.getConfidence());
         canvas.drawText(recog.getTitle() + ": " + recog.getConfidence(), x, y, fgPaint);
        y += fgPaint.getTextSize() * 1.5f;
//        y += fgPaint.getTextSize();
        if (!findImage) {
          if (recog.getTitle().equalsIgnoreCase("soccer ball")) {
            intent.putExtra("webUrl", "http://codigocube.info/6");
            findImage = true;
            break;
          } else if (recog.getTitle().equalsIgnoreCase("hourglass")) {
            intent.putExtra("webUrl", "http://codigocube.info/3");
            findImage = true;
            break;
          } else if (recog.getTitle().equalsIgnoreCase("MORTARBOARD")) {
            intent.putExtra("webUrl", "http://codigocube.info/4");
            findImage = true;
            break;
          } else if (recog.getTitle().equalsIgnoreCase("turnstile")) {
            intent.putExtra("webUrl", "http://codigocube.info/1");
            findImage = true;
            break;
          } else if (recog.getTitle().equalsIgnoreCase("coil") || recog.getTitle().equalsIgnoreCase("globe")) {
            intent.putExtra("webUrl", "http://codigocube.info/2");
            findImage = true;
            break;
          } else if (recog.getTitle().equalsIgnoreCase("joystick")) {
            intent.putExtra("webUrl", "http://codigocube.info/5");
            findImage = true;
            break;
          }
        }
      }
        if(findImage == true) {
          context.startActivity(intent);
          findImage = false;
        }
      }
    }

}
